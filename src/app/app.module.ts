import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { StoreModule as NgRxStoreModule } from '@ngrx/store';
import { reducers } from './store/reducers/main-reducer';
import { Dexie } from 'dexie';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoItemComponent } from './components/todo-item/todo-item.component';
import { TodoHistoryDataComponent } from './components/todo-history-data/todo-history-data.component';
import { AddTodoItemFormComponent } from './components/add-todo-item-form/add-todo-item-form.component';
import { TodoMoviesToWatchComponent } from './components/todo-movies-to-watch/todo-movies-to-watch.component';
import { TodoMovieItemComponent } from './components/todo-movie-item/todo-movie-item.component';
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/user-logged/user-logged.guard';
import { TodoMoviesComponent } from './components/todo-movies/todo-movies.component';
import { MovieDetailComponent } from './components/movie-detail/movie-detail.component';
import { TodoMoviesInfoComponent } from './components/todo-movies-info/todo-movies-info.component';
import { AppLoadService, init_app } from './services/app-load.service';
import { APP_CONFIG_VALUE, APP_CONFIG } from './resources/app-config';
import { DatabaseService } from './services/database.service';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from './services/translations.service';

const childrenMoviesRoutes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: TodoMoviesToWatchComponent },
  { path: 'more-info', component: TodoMoviesInfoComponent },
  { path: ':id', component: MovieDetailComponent }
];

const routes: Routes = [
 { path: '', redirectTo: 'home', pathMatch: 'full' },
 { path: 'home', component: TodoListComponent },
 { path: 'history', component: TodoHistoryDataComponent },
 { path: 'login', component: LoginComponent },
 {
   path: 'protected',
   component: ProtectedComponent,
   canActivate: [ UsuarioLogueadoGuard ]
 },
 {
   path: 'movies',
   component: TodoMoviesComponent,
   children: childrenMoviesRoutes
  }
];

@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    TodoItemComponent,
    TodoHistoryDataComponent,
    AddTodoItemFormComponent,
    TodoMoviesToWatchComponent,
    TodoMovieItemComponent,
    LoginComponent,
    ProtectedComponent,
    TodoMoviesComponent,
    MovieDetailComponent,
    TodoMoviesInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient, DatabaseService]
      }
    })
  ],
  providers: [
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    DatabaseService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
