import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoHistoryDataComponent } from './todo-history-data.component';

describe('TodoHistoryDataComponent', () => {
  let component: TodoHistoryDataComponent;
  let fixture: ComponentFixture<TodoHistoryDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoHistoryDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoHistoryDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
