import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoMoviesToWatchComponent } from './todo-movies-to-watch.component';

describe('TodoMoviesToWatchComponent', () => {
  let component: TodoMoviesToWatchComponent;
  let fixture: ComponentFixture<TodoMoviesToWatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoMoviesToWatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoMoviesToWatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
