import { TodoItemsActionTypes, TodoItemsActions, NewTodoItemAction } from './../actions/todo-items.actions';
import { TodoItem } from './../../models/todo-item.models';

// STATES
export interface TodoItemsState {
    items: TodoItem[];
}

const initializeTodoItems: TodoItemsState = {
    items: [],
};

// REDUCERS
export function reducerTodoItems(state: TodoItemsState = initializeTodoItems, action: TodoItemsActions): TodoItemsState {
    switch (action.type) {
        case TodoItemsActionTypes.NEW_TODO_ITEM:
            return {
                ...state,
                items: [...state.items, (action as NewTodoItemAction).todoItem]
            };
        case TodoItemsActionTypes.DELETE_TODO_ITEMS:
            console.log(state.items);
            return {
                ...state,
                // Devolvemos únicamente los elementos que no esten seleccionados
                items: [...state.items.filter(item => item.selected === false)]
            };
    }
    return state;
}

export function removeTodoItem(arr: TodoItem[], obj: TodoItem): void {
    const i = arr.indexOf(obj);
    if (i !== -1) {
      arr.splice(i, 1);
    }
}

