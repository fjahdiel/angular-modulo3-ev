import { Injectable } from '@angular/core';
import { TranslateLoader, TranslateService } from '@ngx-translate/core';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { DatabaseService, db } from './database.service';
import { HttpClient } from '@angular/common/http';
import { Translation } from '../models/translation.models';
import { APP_CONFIG_VALUE } from '../resources/app-config';

@Injectable({
  providedIn: 'root'
})
export class TranslationsService implements TranslateLoader {
  constructor(private httpClient: HttpClient, private dbManager: DatabaseService) { }
  getTranslation(lang: string): Observable<any> {
    const promise = this.dbManager
      .translations
        .where('lang')
        .equals(lang)
        .toArray()
        .then(results => {
          if (results.length === 0) {
            return this.httpClient
                          .get<Translation[]>(APP_CONFIG_VALUE.endPoint + '/api/translations?lang=' + lang)
                          .toPromise()
                          .then(apiResults => {
                            db.translations.bulkAdd(apiResults);
                            return apiResults;
                          });
          }
          return results;
        }).then((traducciones) => {
          console.log('traducciones cargadas:');
          console.log(traducciones);
          return traducciones;
        }).then((traducciones) => {
          const tr = traducciones.map((t) => ({ [t.key]: t.value} ));
          console.log(tr);
          return tr;
        });
    return from(promise).pipe(flatMap((elems) => from(elems)));
  }
}

export function HttpLoaderFactory(http: HttpClient, dbManager: DatabaseService) {
  return new TranslationsService(http, dbManager);
}
